// Add new custom type in autoform 

AutoForm.addInputType('bbUploadFile', {
	template: 'bbUploadFile', 
	valueOut: function() {
		this.val(); // this is jQuery 
	}
}); 

var getCollection = function(context) {
	if(typeof context.atts.collection == 'string')
		// FS._collections store array of collestions obj
		// FS is global 
		context.atts.collection = FS._collections[context.atts.collection]
	return context.atts.collection; 
};

var getDocument = function(context) {
	var collection = getCollection(context);
	var id = '';
	if(!_.isNull(Template.instance().value)) {
		id = Template.instance().value.get(); // ReactiveVar get 
	}
	if(id != '') {
		return collection.findOne(id);
	} else 
		return false;
	
};

Template.bbUploadFile.onCreated(function() {
	this.value = new ReactiveVar(this.data.value);
});

Template.bbUploadFile.onCreated(function(){
	// check reset 
})

Template.bbUploadFile.helpers({
	label: function() {
		return this.atts.label || 'Choos file upload';
	},
	value: function() {
		var self = this; 
		var document = getDocument(self);
		return (typeof doc !== "undefined" && doc !== null ? doc.isUploaded() : void 0) && doc._id;

	}, 
	schemaKey: function() {
		return this.atts['data-schema-key'];
	},
	previewTemplate: function() {
		var doc = getDocument(this);
		if(doc.isUploaded()) 
			return 'bbFileUploadThumbImg';
	},
	file: function() {
		return getDocument(this);	
	} 
}); 

Template.bbUploadFile.events({
	'click .btn': function(event, template){
		$('.upload-file').click();
	}, 
	'change .upload-file': function(event, template){
		var files = event.target.files; 
		var collection = getCollection(template.data);
		collection.insert(files[0], function(err, fileObj) {
			if(err) {
				console.log('Upload file error');
			} else 
				template.value.set(fileObj._id);
		})
	}
})
