Package.describe({
  name: 'bigbug:autoform-and-file',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: 'Upload form image to specific form',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.1.0.2');
  api.use(['templating', 
            'underscore', 
            'less', 
            'reactive-var', 
            'aldeed:autoform@5.3.0'
        ]);
  api.addFiles(['lib/autoform-and-file.html',
                'lib/autoform-and-file.js', 
                'lib/autoform-and-file.less'
              ], 'client');
});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('bigbug:autoform-and-file');
  api.addFiles('autoform-and-file-tests.js');
});
