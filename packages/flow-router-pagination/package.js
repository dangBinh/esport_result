Package.describe({
  name: 'bigbug:flow-router-pagination',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: 'This package use Boostrap for show ui Pagination just rewrite boostrap ui',
  // URL to the Git repository containing the source code for this package.
  git: '',
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.1.0.2');
  api.use('jquery'); 
  api.use('templating');
  api.use('underscore');
  api.use(['meteorhacks:flow-router'], 'client');
  api.addFiles(['lib/flow-router-pagination.html', 
    'lib/flow-router-pagination.js', 
    'lib/flow-router-pagination.css'], 'client');
});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('bigbug:flow-router-pagination');
  api.addFiles('flow-router-pagination-tests.js');
});
