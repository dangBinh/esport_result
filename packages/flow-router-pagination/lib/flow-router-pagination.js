// Write your package code here!
var pagination = function(total, pageNumber, current) {
	var middle = parseInt(pageNumber / 2); 
	var start = current - middle;
	var end = current + middle;
	if (start <= 0) {
		start = 1;
		if (total < pageNumber) 
			end = total;
		else if(total > pageNumber)
			end = pageNumber;
	} else if(end >= total) {
		end = total; 
		start = end - pageNumber; 
		if (start <= 0) 
			start = 1;
	}

	return _.range(start, end+1);
}

Template.UIPagination.helpers({
	showPrev: function(count){
		var current = parseInt(FlowRouter.getQueryParam('page')) || 1;
		return current != 1;
	}, 
	pages: function(count) {
		// itemPerPage: number item show in page
		// pageNumber: number page show in navigation bar
		var current = parseInt(FlowRouter.getQueryParam('page')) || 1;
		var total = Counts.get(count) / this.itemsPerPage;
		return pagination(total, this.pageNumber, current);
	}, 
	showNext: function(count) {
		var current = parseInt(FlowRouter.getQueryParam('page')) || 1;
		var total = Counts.get(count);
		return current < total;
	}
});

Template.UIPagination.events({
	'click .pagePrev': function(e) {
		e.preventDefault();
		var current = FlowRouter.getQueryParam('page') || 1;
		FlowRouter.setQueryParams({'page':parseInt(current) - 1});
	}, 
	'click .pageNumber': function(e) {
		e.preventDefault();
		var current = FlowRouter.getQueryParam('page') || 1;
		var page = parseInt($(e.target).text()); 
		if (page != current) {
			FlowRouter.setQueryParams({'page': page});
		}
	},
	'click .pageNext': function(e) {
		e.preventDefault();
		var current = FlowRouter.getQueryParam('page') || 1;
		FlowRouter.setQueryParams({'page': parseInt(current) + 1});
	}
})

