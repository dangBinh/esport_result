var collection = new ReactiveVar(null);
var fieldName = new ReactiveVar(null);
var fieldId = new ReactiveVar(null);

Template.btnAddImageToDoc.events({
  "click .btn": function(event, template){
    $('#addImagesToDocModal').modal('show');
    collection.set(template.data.collection);
    fieldName.set(template.data.field);
    fieldId.set(template.data.id);
  }
});

Template.addImagesToDocModal.helpers({
  images: function(){
    return Media.find({});
  }
});

Template.addImagesToDocModal.events({
  "click #btnSelect": function(event, template){
    var collectionName = collection.get();
    var Collection = Mongo.Collection.get(collectionName);
    var field = fieldName.get();
    var id = fieldId.get();
    var modifier = {};
    if(field = "image") {
      modifier.image = this._id;
    }
    Collection.update({_id : id}, {$set : modifier}, {}, function(err, result) {
      $('#addImagesToDocModal').modal('hide');
      if( err )
        return console.log(err);
    })
  }
});
