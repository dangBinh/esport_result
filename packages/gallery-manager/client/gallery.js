var imageSelectedId = new ReactiveVar(null);

Template.GalleryManagerView.events({
  "click #btnGalleryUploadImage": function(event, template){
    $('#galleryModal').modal('show');
  },
  "click #btnGalleryAddAlbum": function(event, template){
    $('#albumModal').modal('show');
  }
});

Template.editImageModal.helpers({
  imageFile: function(){
    var selectedId = imageSelectedId.get();
    if( selectedId ) {
      return Media.find({_id: selectedId});
    }
  }
});

Template.editImageModal.events({
  "click #imageMetadataSave": function(event, template){
    var selectedId = imageSelectedId.get();
    var title = $('#imageTitle').val();
    Media.update({_id: selectedId}, {
                  $set: {
                    "metadata.title": title
                  }
                }, {},function(err, result) {
                  $('#editImage').modal('hide');
                  if(err)
                    return console.log(err);
                })
  }
});

Template.ImagesList.events({
  "click #btnEdit": function(event, template){
    imageSelectedId.set(this._id);
    $('#editImage').modal('show')
  }
});

Template.ImagesList.helpers({
  images: function(){
    return Media.find({});
  }
});

/**
* Upload handle
**/
Template.uploadGalleryImageModal.onCreated(function(){
  this.image = new ReactiveVar(null);
})

Template.uploadGalleryImageModal.events({
  "click .gallery-btn-upload": function(event, template){
    $('.gallery-file').click();
  },
  "change .gallery-file": function(e, t) {
    FS.Utility.eachFile(e, function(file) {
      var fsFile = new FS.File(file)
      fsFile.metadata = {
        title: ''
      };
      t.image.set(fsFile);
      Media.insert(fsFile, function(err, fileObj) {
        $('#galleryModal').modal('hide');
        if(err)
          return console.log(err);
        else
          return console.log(fileObj);
      })
    })
  },
  "dropped .galleryDropzone": FS.EventHandlers.insertFiles(Media, {
      metadata: function (fileObj) {
        return {
          title: ""
        };
      },
      after: function (error, fileObj) {
        $('#galleryModal').modal('hide');
        console.log("Inserted", fileObj.name);
      }
    }),
});

Template.uploadGalleryImageModal.helpers({
  image: function(){
    // check upload progress
    return Template.instance().image.get();
  }
});
