/**
* Alubm collection
**/
AlbumColelction = new Mongo.Collection("gm_album");

AlbumColelction.allow({
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  }
});

/**
 * Media collection
 */
 var createThumb = function(fileObj, readStream, writeStream) {
  //  gm(readStream, fileObj.name()).resize(10, 10).stream().pipe(writeStream);
 }

Media = new FS.Collection("gallery_media", {
  stores: [
    new FS.Store.GridFS("thumbs"),
    new FS.Store.GridFS("gallery_media")
  ],
  filter: {
    allow: {
      ontentTypes: ['image/*']
    }
  }
});

Media.allow({
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  },
  download: function() {
    return true;
  }
});

if(!Meteor.galleryMedia)
  Meteor.galleryMedia = Media;
