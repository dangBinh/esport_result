Package.describe({
  name: 'bigbug:gallery-manager',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: 'Manager gallery in meteor',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.1.0.2');
  api.use('jquery');
  api.use('templating'); // need it to show template in meteor
  api.use('underscore');
  api.use('less');
  api.use('mongo');
  api.use(['cfs:standard-packages',
            'cfs:filesystem',
            'cfs:graphicsmagick',
            'cfs:dropped-event',
            'cfs:ui',
            'reactive-var',
            'dburles:mongo-collection-instances'
        ]);
  api.addFiles('gallery-manager.js');
  api.addFiles(['lib/collection/gallery.js',
              ], ['client', 'server']);
  api.addFiles(['client/gallery.html',
                'client/gallery.js',
                'client/gallery.less',
                'client/button/button.html',
                'client/button/button.js'
              ], 'client');
  api.addFiles(['server/publish.js']
              , 'server');
});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('bigbug:gallery-manager');
  api.addFiles('gallery-manager-tests.js');
});
