/**
 * Class Set add on for react (will be despecrate)
 * @type {[type]}
 */
var cx = React.addons.classSet;

FlashMess = ReactMeteor.createClass({
	templateName: "FlashMess",
	getMeteorState() {
		var flashId = this.props.flashId;
		var message = CgFlashMess.get(flashId);
		return {
			notification: message
		}
	},
	renderMessWithType(message) {
		var alert = 'alert', 
			alertDanger = 'alert-danger',
			alertWarning = 'alert-warning',
			alertInfo = 'alert-info', 
			alertSucess = 'alert-success', 
			alertDismisable = 'alert-dismissable'; 
		if(message.level == 'warning') 
			return cx(alert, alertWarning, alertDismisable); 
		if(message.level == 'danger')
			return cx(alert, alertDanger, alertDismisable);
		if(message.level == 'info')
			return cx(alert, alertInfo, alertDismisable);
		if(message.level == 'success')
			return cx(alert, alertSucess, alertDismisable);
	},
	render() {
		if (this.state.notification) {
			console.log(this.state.notification);
			var classNoti = this.renderMessWithType(this.state.notification);
			return 	<div className={classNoti}>
		                <button type="button" className="close" data-dismiss="alert" aria-hidden="true">×</button>
		                {this.state.notification.message}; 
		            </div>;
	    } else
	    	return false;
	}
})