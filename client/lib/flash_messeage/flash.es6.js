const DEFAULT_ID = '__default__'; 
const DEFAULT_TYPE = 'warning';
// var _deps = Symbol(); user for private but cant handle it
/**
 * Flash messages for meteor wrote in ES2015 and JSX
 * Ref naxio:flash 
 * @type {[type]}
 */
Flash = class Flash {
	constructor(id , messages, timeout, persist) {
		this._deps = new Deps.Dependency(); // add Dep package
		this.id = id || '';
		this.messages = messages || {}; 
		this.id = id || DEFAULT_ID;
		this.timeout = timeout || null; 
		this.persist = persist || false;
	}
	/**
	 * Set infor mation to FlahsMesseage and changed dependency 
	 * @param {[string]} id        [description]
	 * @param {[obj]} messeages [description]
	 * @param {[integer]} timeout   [description]
	 * @param {[boolean]} persist   [description]
	 */
	set (id, message, timeout, persist) {
		var self = this;
		var timeout = timeout || this.timeout;
		this.messages[id] = {
			level: message[0], 
			message: message[1],
			persist: !!persist // change object to boolean
		};
		this._deps.changed(); // invalidate Deps.Dependency
		if (timeout) {
			timer = setTimeout(function(){
				self.clear(id); // remove messeage when have timeout 
				clearTimeout(timer);
			}, timeout);
		}
	}
	/**
	 * 
	 * Get messeages information get messeage by id or by default id 
	 * @param  {[string]} id [description]
	 * @return {[obj]}    [description]
	 */
	get (id) {
		this._deps.depend(); // current computation 
		id = id || DEFAULT_ID; 
		return this.messages[id];
	}
	/**
	 * Clear all information in messeag by id 
	 * @return {[type]} [description]
	 */
	clear(id) {
		var messages = this.messages;
		if(!_.isEmpty(messages)) {
			if (!id) {
				_.each(messages, function(val, key){
					if(_.has(messages, key) && messages[key] != null) {
						if(messages[key].persist)
							messages[key].persist = false; 
						else 
							messages[key] = null;
					}
				})
			} else {
				if(messages[id].persist) {
					messages[id].persist = false;
				} else 
					messages[id] = null;
			}
			this._deps.changed();
		}
	}
	warning(id, message, timeout, persist) {
		return this.set(id, ['warning', message], timeout, persist);
	}
	success(id, message, timeout, persist) {
		return this.set(id, ['success', message], timeout, persist);
	}
	info(id, message, timeout, persist) {
		return this.set(id, ['info', message], timeout, persist);
	}
	danger(id, message, timeout, persist) {
		return this.set(id, ['danger', message], timeout, persist);
	}
}

/**
 * CgFlashMess.warning(id ,message, timeout, persist); 
 * CgFlashMess.info(id ,message, timeout, persist);
 * CgFlashMess.success(id ,message, timeout, persist);
 * CgFlashMess.danger(id ,message, timeout, persist);
 */
if ('undefined' === typeof CgFlashMess) 
	CgFlashMess = new Flash();

// Router Injection with Hook
Meteor.startup(function(){
	if(typeof(Router) !== 'undefined' && Router.routes) {
		Router.onBeforeAction(function(){
			CgFlashMess.clear(); 
			this.next();
		})
	}
})
