/**
 * Will be depecrated
 * @type {[type]}
 */
var cx = React.addons.classSet;

// sample data
// show arrow if had child
// ul class nav nav-second-level collapse in
var siderbarMenu = [
	{id: "1", title: "Dashboard", url: "/admin", icon: "fa-dashboard"},
	{id: "2", title: 'User', url: "/admin/user", icon: "fa-user"},
	{id: "3", title: 'Game Cateogry', url: "/admin/games", icon: 'fa-gamepad'},
	{id: "4", title: "Events", url: '/admin/events', icon: 'fa-list'},
	{id: "5", title: "Matches", url: '/admin/matches', icon: 'fa-child'},
	{id: "6", title: "Team", url: null, icon: 'fa-circle',
		children: [
			{id: '7', title: 'Manage Team', url: '/admin/teams', icon: 'fa-circle'},
			{id: '8', title: 'Manage Player', url: '/admin/players', icon: 'fa-circle'}
		]
	},
	{id: "9", title: "Gallery", url: '/admin/gallery', icon: 'fa-picture-o '},
];

/**
 * - Tree View
 * 		- Tree Node
 * 			- Children
 * 	Siderbar search will be handled
 * @param  {[type]} options.templateName:    "ReactMeteorSiderbar" [description]
 * @param  {[type]} options.getMeteorState() {                    [description]
 * @param  {[type]} render(                  [description]
 * @return {[type]}                          [description]
 */
 ReactMeteorSiderbar = ReactMeteor.createClass({
 	templateName: "ReactMeteorSiderbar",
 	getInitialState() {
 		return {
 			data: siderbarMenu,
 		}
 	},
 	getDefaultProps() {
 		return {
 			children: []
 		}
 	},
 	onActive(node) {
	 	if (this.state.actived && this.state.actived.isMounted()) {
            this.state.actived.setState({actived: false});
        }
        this.setState({actived: node});
        node.setState({actived: true});
        if (this.props.onItemActive) {
            this.props.onItemActive(node);
        }

 	},
 	renderTreeNode(child) {
 		var self = this;
 		return <TreeNode
 				key={child.id}
 				data={child}
 				onItemActive={self.onActive}
 				/>;
 	},
 	render() {
 	 	return <ul className="nav in" id="side-menu">
            <li className="sidebar-search">
                <div className="input-group custom-search-form">
                    <input type="text" className="form-control" placeholder="Search..." />
                    <span className ="input-group-btn">
                    <button className="btn btn-default" type="button">
                        <i className="fa fa-search"></i>
                    </button>
                </span>
                </div>
            </li>
        	/*Search bar will be handle late*/
        	{
        		this.state.data.map(this.renderTreeNode)
        	}
        </ul>;
 	 }
 })

var TreeNode = ReactMeteor.createClass({
	getInitialState() {
		return {
		};
	},
	getDefaultProps() {
		return {
			children: []
		};
	},
	onExpandation(e) {
		this.setState({expand: !this.state.expand});
		e.preventDefault();
		e.stopPropagation();
	},
	onItemActive(e) {
		if(this.props.onItemActive) {
			this.props.onItemActive(this);
		}
		// e.preventDefault();
		// e.stopPropagation();
	},
	render() {
		var children = [];
		var active = null;
		var liActive = null;
		var arrowIcon = null;
		var {title, url, ...rest} = this.props.data;
		var faIcon = rest.icon;
		var icon = cx(
			"fa",
			faIcon,
			"fa-fw"
		);
		var collapse = cx({
			'nav nav-second-level collapse': true,
			'in': (this.state.expand ? true : false)
		});
		var expandation = cx({
			'active': (this.state.expand ? true : false)
		});
		if(!_.has(rest, 'children')) {
			rest.children = [];
			var active = cx({
				'active': (this.state.actived ? true : false)
			});
		} else {
			children.push(
				<ul className={collapse}>
                	{
                		rest.children.map(function(child){
                			return <TreeNode
                				key={child.id}
                				data={child}
                				onItemActive={this.props.onItemActive}
                				/>;
                		}.bind(this))
                	}
                </ul>
				);
            arrowIcon = React.createElement('span', {className: 'fa arrow'});
		}
		return <li className={expandation} onClick={this.onExpandation}>
                <a data-url={url} className={active} onClick={this.onItemActive}>
                	<i className={icon}></i>
                	{title}
                	{arrowIcon}
            	</a>
            	{children}
            </li>;
	}
})
