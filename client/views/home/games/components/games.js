Template.GamesListTemplate.helpers({
  games: function(){
    return Games.find({});
  }
});

Template.MatchInGameListTemplate.helpers({
  game: function(){
    return Games.find({});
  },
  matches: function() {
    return Matches.find({game: this._id});
  },
  gameType: function() {
		// this == matches
		if(this.matchType == 2) {
			return true;
		} else {
			return false;
		}
	},
	teeamInMatchDetail: function() {
		return Teams.find({_id: {$in: this.teams}}).fetch();
	},
	playerInMatchDetail: function() {
		return Players.find({_id: {$in: this.teams}}).fetch();
	}

});
