var cx = React.addons.classSet;

// static data
var ReactCommentBox = ReactMeteor.createClass({
  templateName: "ReactCommentBox",
  startMeteorSubscriptions: function() {
    // subscription for comments
   // use in FlowRouter
  },
  getMeteorState: function() {
    var slug = FlowRouter.current().params.slug,
    event = Events.findOne({slug : slug}),
    comments = Comments.find({_id : event.commentsId});
    console.log(comments.count());
    if(comments.count() <= 0) {
      comments = [];
    } else {
      comments = comments.fetch();
      comments = comments[0];
    };
    var currentUser = (!_.isNull(Meteor.userId())) ? true : false;
    return {
      curSlug: slug,
      commentsList : comments.comments,
      commentsId : comments._id,
      isLoggedIn : currentUser
    }
  },
  onCommentSubmit(data) {
    var comments =this.state.commentsList || [];
    var curSlug = this.state.curSlug;
    if(comments.length <= 0) {
    var id =  Comments.insert({
          comments : [
            { content : data.data }
         ]
      }, function(err, _id) {
        var commentId = _id;
        if(err)
          return console.log(err);
        return true;
      });
      var event = Events.findOne({slug : curSlug});
      Events.update({_id:event._id}, {$set : { commentsId  : id}}, {}, function(err) {
        if(err)
          console.log(err);
      });
    } else {
      Comments.update({_id : this.state.commentsId},
                      { $push: { comments :  {content : data.data} } }
                    );
    };
    // Comment.upsert({data.text});
  },
  onReplySubmit(data) {
      var position = data.index;
      var dyn = {};
      var selector = "comments." + position + ".replies";
      if(data.replies.length <= 0) {
        dyn[selector] =  [{content : data.data}] ;
        Comments.update({_id : this.state.commentsId},
                        {
                          $set : dyn
                        }
                      );
        } else {
          dyn[selector] =  {content : data.data} ;
          Comments.update({_id : this.state.commentsId},
                          {
                            $push : dyn
                          }
                        );
        }
  },
  render: function() {
    var children = [];
    var commentsLIst = this.state.commentsList || [];
    if(this.state.isLoggedIn)
      replyStatus = true;
    else
      replyStatus = false;
    if(commentsLIst.length > 0)
      children.push(<ReactCommentList data={commentsLIst} reply={replyStatus} onReplySubmit={this.onReplySubmit} isLoggedIn={this.state.isLoggedIn} />);
    if(this.state.isLoggedIn)
      children.push(<ReactCommentForm onCommentSubmit={this.onCommentSubmit} />)
    return <div className="comment-box">
      {children}
    </div>;
  }
})

var ReactCommentList = ReactMeteor.createClass({
  render: function() {
    var reply = this.props.reply;
    var comments = this.props.data.map(function(comment, index){
      var replies = comment.replies || false;
      var user = Meteor.users.findOne({_id : comment.owner});
      return <ReactComment owner ={user}
                          content={comment.content}
                          replies={replies}
                          createdAt={comment.createdAt}
                          reply = {reply}
                          index = {index}
                          onReplySubmit={this.props.onReplySubmit}
         />
     }.bind(this));
    return <ul className="commnet-list">
            {comments}
          </ul>;
  }
});

var ReactComment = React.createClass({
  getInitialState() {
    return {
      showCommentForm : false
    }
  },
  clickReply(e) {
    e.preventDefault();
    this.setState({showCommentForm : true})
  },
  closeReply() {
    this.setState({showCommentForm : false})
  },
  render: function() {
    var replies = this.props.replies || [];
    var children = [];
    var replyBtn = [];
    var userImage = UserImage.findOne({owner : this.props.owner._id});
      if(typeof(userImage) != 'undefined') {
        var userImageurl = userImage.url("userThumb");
      } else {
        var userImageurl = null;
      }

    if(this.props.reply === true)
      replyBtn.push(
        <p>
          <a href="#" className="btnReply" onClick={this.clickReply}>Reply</a>
        </p>
      );
    if(replies.length > 0) {
      children.push(<ReactCommentList data={replies} reply="false"/>);
    }
    if(this.state.showCommentForm) {
      children.push(<ReactCommentForm
        commentIndex={this.props.index}
        onReplySubmit={this.props.onReplySubmit}
        closeReply={this.closeReply}
        replies = {replies}
        />)
    }
    var b = new Date(),
      a = this.props.createdAt;
    var time = moment(a).from(moment(b), true);
    return   <li className="clearfix">
      <ReactAvatar userId={this.props.owner._id}
        cfsImage={userImageurl}
        shape="rounded"
        class="avatar"
        />
      <div className="comment-detail">
        <p className="comment-user-name"><b>{this.props.owner.username}</b> - {time}</p>
        <p className="comment-content">{this.props.content}</p>
        {replyBtn}
      </div>
      { children }
    </li>
    ;
  }
})

var ReactCommentForm = React.createClass({
  handleCommentSubmit: function(e) {
    e.preventDefault();
    var commentText = React.findDOMNode(this.refs.comment).value.trim();
    var commentIndex = this.props.commentIndex;
    var replies = this.props.replies
    if (typeof this.props.onCommentSubmit === "function")
      this.props.onCommentSubmit({data : commentText});
    else if (typeof this.props.onReplySubmit === "function" ) {
      this.props.onReplySubmit({data : commentText, index : commentIndex, replies : replies})
      this.props.closeReply();
    }
  },
  render: function() {
    return  <form className="comment-form" role="form" onSubmit={this.handleCommentSubmit}>
            <div className="row">
              <div className="col-lg-12">
                <div className="input-group input-group-lg">
                  <input className="form-control input-lg" type="text" placeholder="Your comments" ref="comment"/>
                 <span className="input-group-btn">
                      <button className="btn btn-default btn-lg">Add</button>
                  </span>
                </div>
              </div>
            </div>
        </form>;
  }
})
