Template.Header.events({
  "click #btnUserLogin": function(event, template){
    $("#userLoginModal").modal('show');
  },
  "click #btnUserLogout": function(event, template) {
    Meteor.logout();
    FlowRouter.go('/');
  }
});
Template.userLoginModal.onCreated(function(){
  this.formType = new ReactiveVar('signin');
})
Template.userLoginModal.helpers({
  getForm: function(){
    var formType = Template.instance().formType.get();
    if(formType == 'signin')
      return 'userLoginForm';
    else if(formType == 'forgot')
      return 'userForgotPasswordForm';
    else if(formType == 'registration')
      return 'userCreateForm';
  },
  formHeader: function() {
    var formType = Template.instance().formType.get();
    if(formType == 'signin')
      return 'Sign In';
    else if(formType == 'forgot')
      return 'Forgot Password';
    else if(formType == 'registration')
      return 'Sign Up';
  }
});

Template.userLoginModal.events({
  "click #linkForgotPassword": function(event, template){
    template.formType.set('forgot');
  },
  "click .registration": function(event, template) {
    template.formType.set('registration');
  },
  "click #backToSignIn": function(event, template) {
    console.log(this);
    template.formType.set('signin');
  }
});

Template.userLoginForm.events({
  'submit form': function(event, template) {
    event.preventDefault();
    var currentTarget = event.currentTarget;
    var userVar = $(currentTarget).find('#user-name').val();
    var passVar = $(currentTarget).find('#user-pass').val();
    var user = s(userVar).trim().value();
    var password = s(passVar).trim().value();
    Meteor.loginWithPassword(user, password, function(err) {
      if(err)
        return console.log(err);
      $("#userLoginModal").modal('hide');
    });
  }
});
Template.userCreateForm.events({
  'submit form': function(event, template) {
    event.preventDefault();
    var currentTarget = event.currentTarget;
    var userVar = $(currentTarget).find('#user-name').val();
    var emailVar = $(currentTarget).find('#user-email').val();
    var passVar = $(currentTarget).find('#user-pass').val();
    var repassVar = $(currentTarget).find('#user-repass').val();
    var pass = s(passVar).trim().value();
    var repass = s(repassVar).trim().value();
    if(pass == repass && pass != '') {
      var user = s(userVar).trim().value();
      var email = s(emailVar).trim().value();
      var user = {
        username: user,
        email : email,
        password : pass
      };
      Accounts.createUser(user, function(err) {
        if(err)
          return console.log(err); // use flassh mess here
        else {
          $("#userLoginModal").modal('hide');
          return true;// send email here
        }
      })
    }
  }
})
