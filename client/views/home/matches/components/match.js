Template.MatchDetailTemplate.helpers({
	match: function() {
		return Matches.find({});
	},
	gameType: function() {
		// this == matches
		if(this.matchType == 2) {
			return true;
		} else {
			return false;
		}
	},
	teeamInMatchDetail: function() {
		return Teams.find({_id: {$in: this.teams}}).fetch();
	},
	playerInMatchDetail: function() {
		return Players.find({_id: {$in: this.teams}}).fetch();
	},
});

// Template.linkBtnAction.helpers({
// 	docChannel: function(){
// 		 var id = editChannelId.get() || false;
// 		 console.log(id);
// 		 if( id ) {
// 		 	return Channels.findOne({_id:id});
// 		 }
// 		 return false;
// 	}
// });
