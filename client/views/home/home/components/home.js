Template.MainContent.helpers({
	events: function() {
		return Events.find({})
	},
	image: function() {
		var imageId = this.image || false;
		if(imageId)
			return Meteor.galleryMedia.findOne({_id : this.image})
		return false;
	}
})
