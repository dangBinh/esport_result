Template.UserImageProfile.events({
  'change #jsInputFile': function(event, template) {
      FS.Utility.eachFile(event, function(file) {
        var fsFile = new FS.File(file);
        fsFile.owner = Meteor.userId();
        var imgId = UserImage.insert(fsFile, function (err, fileObj) {
          if(err)
            return console.log(err);
        });
      });
    }
 });

 Template.UserImageProfile.helpers({
   userImage: function(){
     var userImage = UserImage.findOne({owner : Meteor.userId()});
     if (typeof userImage != 'undefined')
      return userImage;
    return false;
   }
 });

 Template.UserChangeInfo.helpers({
   profile: function(){
    //  return Meteor.user().
   }
 });

 Template.avatar.helpers({
   imageUrl: function () {
    var user = this.user ? this.user : Meteor.users.findOne(this.userId);
    var userImage = UserImage.findOne({owner : Meteor.userId()});
    var url = Avatar.getUrl(user);
    if(userImage != null)
      url = userImage.url("userThumb");
    if (url && url.trim() !== '' && Template.instance().firstNode) {
      var img = Template.instance().find('img');
      if (img.src !== url.trim()) {
        img.style.removeProperty('display');
      }
    }
    return url;
  }
 });

 Template.UserChangeInfo.events({
   "submit form": function(event, template){
     event.preventDefault();
     var firstName = $(event.currentTarget).find("#firstName").val(),
     lastName = $(event.currentTarget).find("#lastName").val();
     console.log(firstName, lastName);
     if (Meteor.userId() != null) {
       Meteor.users.update( { _id: Meteor.userId() },
                             { $set:
                                    {"profile.firstName":firstName,
                                      "profile.lastName": lastName}
                                } );
     }
   }
 });

 Template.UserChangePassword.events({
   "submit form": function(event, template){
     event.preventDefault();
     var oldPass = $(event.currentTarget).find("#oldPassword").val(),
     pass = $(event.currentTarget).find("#password").val(),
     confirmPass = $(event.currentTarget).find("#confirmPassword").val();
     console.log(oldPass, pass, confirmPass);
     if (Meteor.userId() != null && pass === confirmPass) {
      Accounts.changePassword(oldPass, pass, function(err){
          if(err)
            return console.log(err);
          $(event.currentTarget).trigger('reset');
        });
      }
    }
 });
