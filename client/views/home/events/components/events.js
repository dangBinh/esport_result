var editChannelId = new ReactiveVar(null);
var chooseLink = new ReactiveVar(null);
Template.EventDetailTemplate.onRendered(function(){
	chooseLink.set(null);
})

Template.EventDetailTemplate.helpers({
	shareData: function() {
	 return {
		 title: this.data
	}
 },
	event: function() {
		var slug = FlowRouter.current().params.slug;
		return Events.findOne({slug:slug});
	},
	// matchesInEvent: function() {
	// 	return Matches.find({_id: {$in : this.matchesId}});
	// },
	gameType: function() {
		// this == matches
		console.log(this.matchType);
		if(this.matchType == 2) {
			return true;
		} else {
			return false;
		}
	},
	teeamInMatchDetail: function() {
		return Teams.find({_id: {$in: this.teams}}).fetch();
	},
	playerInMatchDetail: function() {
		return Players.find({_id: {$in: this.teams}}).fetch();
	},
	channelsLink: function() {
		// this == match
		var channelsId = this.channelsId || [];
		if (channelsId != 0) {
			var collection = Channels.find({_id: { $in : channelsId}}).fetch();
			var list = [];
			/**
			*  [
			*  	{name: English, linkArray : [links]}
			*  ]
			*/
			var list = [
				{name: 'English', linkArray: []},
				{name: 'Vietnamese', linkArray: []}
			];
			_.each(collection, function(el, key){
				var id = el._id;
				var detail = el.channel;
				getListLangugae('en', 'English', list, id, detail);
				getListLangugae('vi', 'Vietnamese', list, id, detail);
			})

			_.each(list, function(el, key) {
				var array = el.linkArray || [];
				if (array.length <= 0) {
					list.splice(key, 1);
				}
			})

			return list;
		}
		return false;
	},
	docChannel: function(){
		 var id = editChannelId.get() || false;
		 if( id ) {
			return Channels.findOne({_id:id});
		 }
		 return false;
	},
	emberVideo: function() {
		if(chooseLink.get() != null)
			return Spacebars.SafeString(chooseLink.get());
		else
			return false;
	}
});

var getListLangugae = function(langVal, lang, list, id, detail) {
	if(detail.language == langVal)
		_.each(list, function(el, key){
			if(el.name == lang) {
				var options = {};
				options._id = id;
				options.link = detail.link;
				options.ember = detail.ember;
				options.owner = (Meteor.userId() == detail.owner) ? true : false;
				list[key].linkArray.push(options);
			}
		})
}

Template.EventRightDS.helpers({
	event: function(){
		var event = Template.instance().data.dataEvent;
		return event;
	},
	mapOptions: function() {
		var event = Template.instance().data.dataEvent;
		console.log(event);
		if(GoogleMaps.loaded()) {
		return {
          center: new google.maps.LatLng(event.location.lat, event.location.lng),
          zoom: 8
        };
		}
	}
});

Template.EventRightDS.onCreated(function() {
    // We can use the `ready` callback to interact with the map API once the map is ready.
    GoogleMaps.ready('map', function(map) {
      // Add a marker to the map once it's ready
      var marker = new google.maps.Marker({
        position: map.options.center,
        map: map.instance
      });
    });
  });

Template.EventDetailTemplate.events({
	"click #getLink": function(event, template) {
		chooseLink.set(this.ember);
	},
});

Template.linkBtnAction.events({
	"click #editChannel": function(event, template){
		editChannelId.set(template.data._id);
	},
	'click #deleteChannel': function(event, template) {
		var currentSlug = FlowRouter.current().params.slug;
		Meteor.call("deleteChannel", template.data._id, currentSlug, function(error, result){
			if(error){
				console.log("error", error);
			}
		});
	}
});

AutoForm.addHooks(
	['insertChannelForm'],
	{
		onError: function(type, err) {
			console.log(err);
		},
		onSuccess: function(type, doc) {
			$('#myModal').modal('hide')
			var current = FlowRouter.current();
			var slug = current.params.slug;
			var eventsDoc = Events.findOne({slug: slug});
			Events.update(
				{_id: eventsDoc._id},
				{ $addToSet: { channelsId: doc } }
			);
		}
	}
);

AutoForm.addHooks(
	['updateChannelForm'],
	{
		onError: function (type, err) {
			console.log(err);
		},
		onSuccess: function(type, doc) {
			$('#editModal').modal('hide');
		}
	}
)
