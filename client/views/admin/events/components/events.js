Template.AdminEventsTable.helpers({
	events: function() {
		return Events.find({});
	},
	image: function() {
		if(typeof this.image != undefined)
			return Meteor.galleryMedia.findOne({_id : this.image});
		else
			return false;
	}
})

Template.AdminEventsTable.events({
	'click #btnNew': function(event, template) {
		FlowRouter.go('/admin/events/add');
	},
	'click #btnEdit': function(event, template) {
		FlowRouter.go('/admin/events/edit?id='+this._id);
	},
	'click #btnDelete': function(event, template) {
		Meteor.call('removeEvent', this._id);
	}
});

Template.AdminEventForm.helpers({
	addOrEdit: function() {
		var action = FlowRouter.getParam('action');
		if (action == 'add') {
			return'insertAdminEventForm';
		} else if(action == 'edit'){
			return 'updateAdminEventForm';
		}
	}
});

Template.updateAdminEventForm.helpers({
	docEvent: function() {
		var eventId = FlowRouter.getQueryParam('id');
		return Events.findOne({_id: eventId});
	}
});
//user hook in client side

AutoForm.addHooks(['insertAdminEventForm'], {
	onError: function(type, err) {
		console.log(err);
	},
	onSuccess: function(formType, result) {
		if(formType == 'insert') {
			FlowRouter.go('/admin/events');
		}
	}
});

AutoForm.addHooks(['updateAdminEventForm'], {
	onSuccess: function(formType, result) {
		if(formType == 'update' && result == 1) {
			FlowRouter.go('/admin/events');
		}
	}
});

Template.registerHelper('formatDate', function(date) {
  	return moment(date).format('MM-DD-YYYY');
});
