Template.AdminMatchesTable.helpers({
	matches: function() {
		return Matches.find({});
	}
})

Template.AdminMatchesTable.events({
	'click #btnNew': function(event, template) {
		FlowRouter.go('/admin/matches/add');
	}, 
	'click #btnEdit': function(event, template) {
		FlowRouter.go('/admin/matches/edit?id='+this._id);
	},
	'click #btnDelete': function(event, template) {
		Meteor.call('removeMatch', this._id);
	}
}); 

Template.AdminMatchForm.helpers({
	addOrEdit: function() {
		var action = FlowRouter.getParam('action'); 
		if (action == 'add') {
			return'insertAdminMatchForm';
		} else if(action == 'edit'){
			return 'updateAdminMatchForm';
		} 
	}
});

Template.insertAdminMatchForm.onCreated(function(){
	this.matchType = new ReactiveVar(null);
	this.tpLabel = new ReactiveVar(null);
	this.winner = new ReactiveVar(null);
})

Template.insertAdminMatchForm.events({
	'change .matchType': function(event, template) {
		var type = $(event.target).val();
		template.matchType.set(type);
		if(type == 1) {
			template.tpLabel.set('Players');
		} else {
			template.tpLabel.set('Teams');
		}
	}, 
	'change .teams': function(event, template) {
		var val = $(event.target).val();
		var listTeam = [];
		_.each(val, function(el , value){
			var label = $(event.target).find('option[value='+el+']').text();
			listTeam.push({label: label, value: el})
		});
		template.winner.set(listTeam);
	}
});

Template.insertAdminMatchForm.onCreated(function(){
	this.matchType = new ReactiveVar(null);
	this.tpLabel = new ReactiveVar(null);
	this.winner = new ReactiveVar(null);
})

Template.insertAdminMatchForm.helpers({
	teamOrPlayer: function() {
		if(Template.instance().matchType.get() == 1) {
			return getPlayersList();
		} else {
			return getTeamList();
		}
	}, 
	tpLabel: function() {
		return Template.instance().tpLabel.get();
	}, 
	winnerChoice: function() {
		return Template.instance.winner.get();
	} 
})

var getPlayersList = function() {
	return Players.find({}).map(function(player) {
		return {label: player.nickName, value: player._id}
	})
}

var getTeamList = function() {
	return Teams.find({}).map(function(team) {
		return {label: team.name, value: team._id}
	})
}

Template.updateAdminMatchForm.onCreated(function(){
	this.matchType = new ReactiveVar(null);
	this.tpLabel = new ReactiveVar(null);
	this.winner = new ReactiveVar(null);
})

Template.updateAdminMatchForm.onRendered(function(){
	var val = this.$('.matchType').val();
	Template.instance().matchType.set(val);
	if(val == 1) {
		Template.instance().tpLabel.set('Players');
	} else {
		Template.instance().tpLabel.set('Teams');
	}
})

Template.updateAdminMatchForm.helpers({
	teamOrPlayer: function() {
		if(Template.instance().matchType.get() == 1) {
			return getPlayersList();
		} else {
			return getTeamList();
		}
	},
	tpLabel: function() {
		return Template.instance().tpLabel.get();
	},  
	docMatch: function() {
		var matchId = FlowRouter.getQueryParam('id');
		return Matches.findOne({_id: matchId});
	}
});
//user hook in client side

AutoForm.addHooks(['insertAdminMatchForm'], {
	onError: function(type, err) {
		console.log(err);
	},
	onSuccess: function(formType, result) {
		if(formType == 'insert') {
			FlowRouter.go('/admin/matches');
		}
	}
}); 

AutoForm.addHooks(['updateAdminMatchForm'], {
	onSuccess: function(formType, result) {
		if(formType == 'update' && result == 1) {
			FlowRouter.go('/admin/matches');
		}
	}
});

Template.registerHelper('formatDate', function(date) {
  	return moment(date).format('MM-DD-YYYY');
});




