Template.AdminTeamsTable.helpers({
	teams: function() {
		return Teams.find({});
	},
	image: function() {
		return Meteor.galleryMedia.findOne({_id : this.image});
	}
})

Template.AdminTeamsTable.events({
	'click #btnNew': function(event, template) {
		FlowRouter.go('/admin/teams/add');
	},
	'click #btnEdit': function(event, template) {
		FlowRouter.go('/admin/teams/edit?id='+this._id);
	},
	'click #btnDelete': function(event, template) {
		Meteor.call('removeTeam', this._id);
	}
});

Template.AdminTeamForm.helpers({
	addOrEdit: function() {
		var action = FlowRouter.getParam('action');
		if (action == 'add') {
			return'insertAdminTeamForm';
		} else if(action == 'edit'){
			return 'updateAdminTeamForm';
		}
	}
})

Template.updateAdminTeamForm.helpers({
	docTeam: function() {
		var teamId = FlowRouter.getQueryParam('id');
		return Teams.findOne({_id: teamId});
	}
});
//user hook in client side

AutoForm.addHooks(['insertAdminTeamForm'], {
	onSuccess: function(formType, result) {
		if(formType == 'insert') {
			FlowRouter.go('/admin/teams');
		}
	}
});

AutoForm.addHooks(['updateAdminTeamForm'], {
	onSuccess: function(formType, result) {
		if(formType == 'update' && result == 1) {
			FlowRouter.go('/admin/teams');
		}
	}
});
