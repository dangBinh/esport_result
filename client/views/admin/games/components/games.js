Template.AdminGamesTable.helpers({
	games: function() {
		return Games.find({});
	},
	image: function() {
		return Meteor.galleryMedia.findOne({_id : this.image});
	}
})

Template.AdminGamesTable.events({
	'click #btnNew': function(event, template) {
		FlowRouter.go('/admin/games/add');
	},
	'click #btnEdit': function(event, template) {
		FlowRouter.go('/admin/games/edit?id='+this._id);
	},
	'click #btnDelete': function(event, template) {
		Meteor.call('removeGame', this._id);
	}
});

Template.AdminGamesForm.helpers({
	addOrEdit: function() {
		var action = FlowRouter.getParam('action');
		if (action == 'add') {
			return'insertAdminGamesForm';
		} else if(action == 'edit'){
			return 'updateAdminGamesForm';
		}
		return 'insertAdminGamesForm';
	}
})

Template.updateAdminGamesForm.helpers({
	docGame: function() {
		var gameId = FlowRouter.getQueryParam('id');
		console.log(Games.findOne({_id: gameId}));
		return Games.findOne({_id: gameId});
	}
});
// user hook in client side
AutoForm.addHooks(['insertAdminGamesForm'], {
	onSuccess: function(formType, result) {
		console.log(formType);
		if(formType == 'insert') {
			FlowRouter.go('/admin/games');
		}
	}
});

AutoForm.addHooks(['updateAdminGamesForm'], {
	onSuccess: function(formType, result) {
		if(formType == 'update' && result == 1) {
			FlowRouter.go('/admin/games');
		}
	}
});
