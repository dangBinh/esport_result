Template.AdminPlayersTable.helpers({
	players: function() {
		return Players.find({});
	},
	image: function() {
		return Meteor.galleryMedia.findOne({_id : this.image});
	}
})

Template.AdminPlayersTable.events({
	'click #btnNew': function(event, template) {
		FlowRouter.go('/admin/players/add');
	},
	'click #btnEdit': function(event, template) {
		FlowRouter.go('/admin/players/edit?id='+this._id);
	},
	'click #btnDelete': function(event, template) {
		Meteor.call('removePlayer', this._id);
	}
});

Template.AdminPlayerForm.helpers({
	addOrEdit: function() {
		var action = FlowRouter.getParam('action');
		if (action == 'add') {
			return'insertAdminPlayerForm';
		} else if(action == 'edit'){
			return 'updateAdminPlayerForm';
		}
	}
})

Template.insertAdminPlayerForm.helpers({
	getCountires: function() {

	}
})

Template.updateAdminPlayerForm.helpers({
	docPlayer: function() {
		var playerId = FlowRouter.getQueryParam('id');
		return Players.findOne({_id: playerId});
	}
});
//user hook in client side

AutoForm.addHooks(['insertAdminPlayerForm'], {
	onSuccess: function(formType, result) {
		if(formType == 'insert') {
			FlowRouter.go('/admin/players');
		}
	}
});

AutoForm.addHooks(['updateAdminPlayerForm'], {
	onSuccess: function(formType, result) {
		if(formType == 'update' && result == 1) {
			FlowRouter.go('/admin/players');
		}
	}
});
