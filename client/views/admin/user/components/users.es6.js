var userId = new ReactiveVar(null);

Template.AdminUserTable.helpers({
	users: () => {
		return Meteor.users.find({_id: {$ne: Meteor.userId()}});
	}
});

Template.AdminUserTable.events({
	'click #btnNew': function(e) {
		userId.set(null);
	},
	'click #btnEdit': function(e) {
		e.preventDefault();
		return userId.set(this._id);
	},
	'click #btnDelete': function(e, t) {
		Meteor.call('deleteUserInAdmin', this._id);
	}	
});

Template.AdminUserEditForm.helpers({
	addOrEdit: function(){ 
		if(!_.isNull(userId.get())) {
			this.userId = userId.get();
			return false;
		}
		return true;
	}, 
	log: function() {
		console.log(this);
	}
})

Template.updateUserForm.helpers({
	docUser: function() {
		return Meteor.users.findOne({_id: userId.get()});
	}, 
	file: function() {
		// user collection helper
		if(!_.isNull(userId.get())) {
			var user = Meteor.users.findOne({_id: userId.get()});
			if(typeof user != undefined) {
				if(typeof user.profile.images != undefined) {
					var imagesId = user.profile.images;
					return UserImage.findOne({_id: imagesId});
				}
			}
		}
		return false;
	}
}); 

/**
 * Cannot update image in autofrm fuck this 
 */


AutoForm.addHooks(
  ["updateUserForm"],
  {
    before   : {
      update: CfsAutoForm.Hooks.beforeUpdate
    },
    after    : {
      update: CfsAutoForm.Hooks.afterUpdate
    }
  }
);
// Debug autoform 

// AutoForm.addHooks(['updateUserForm'], {
//     before: {
//       insert: function(error, result) {
//         if (error) {
//           console.log("Insert Error:", error);
//           // AutoForm.debug();
//         } else {
//           console.log("Insert Result:", result);
//           // AutoForm.debug();
//         }
//       },
//       update: function(error) {
//         if (error) {
//           console.log("Update Error:", error);
//           // AutoForm.debug();
//         } else {
//           console.log("Updated!");
//           console.log('AutoForm.debug()');
//         }
//       }
//     }
//   });
// AutoForm.debug();
