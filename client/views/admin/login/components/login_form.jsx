var LoginForm = ReactMeteor.createClass({
	templateName: "LoginForm",
	getInitialState() {
		return {
			flashId: '__default__'
		}
	},
	handleSubmit(e) {
		e.preventDefault();
		var self = this;
		var email = React.findDOMNode(this.refs.email).value.trim();
		var password = React.findDOMNode(this.refs.password).value.trim();
		var remeberMe = React.findDOMNode(this.refs.remember_me).value;
		Meteor.loginWithPassword(email, password, function (err) {
			if(err) {
				// show err 
				self.setState({
					flashId: 'login_error'
				});
				self.reset();
				CgFlashMess.danger('login_error', 'Wrong apssword or email');
			} else {
				return FlowRouter.go('/admin');
			}
		});
	},
	reset() {
		// reset uncontroller input 
		this.refs.email.getDOMNode().value = ''; 
		this.refs.password.getDOMNode().value = '';
	},
	render() {
		var flashId = this.state.flashId;
		return <form onSubmit={this.handleSubmit} role="form">
	                <fieldset>
	                    <div className="form-group">
	                        <input className="form-control" placeholder="E-mail" name="email" type="email" ref="email" autofocus />
	                    </div>
	                    <div className="form-group">
	                        <input className="form-control" placeholder="Password" name="password" type="password" ref="password" />
	                    </div>
	                    <div className="checkbox">
	                        <label>
	                            <input name="remember" type="checkbox" value="Remember Me" ref="remember_me" />Remember Me
	                        </label>
	                    </div>
	                    <button type="submit" className="btn btn-lg btn-success btn-block">Login</button>
	                </fieldset>
	                <FlashMess flashId={flashId}/> 
	            </form>;
	}
}) 