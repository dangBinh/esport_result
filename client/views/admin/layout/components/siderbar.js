Template.AdminSiderbar.events({
	'click li>a': function(e) {
		var url = $(e.target).attr('data-url');
		if(url) 
			FlowRouter.go(url);
	}
})
