// HOme group router
var homeRouter = FlowRouter.group ({
	prefix: '/',
	triggersEnter: [
	 (context, redirect) => {
	 	Blaze.addBodyClass('home');
	 }
	]
});

FlowRouter.route('/', {
	triggersEnter: [
	 (context, redirect) => {
		Blaze.addBodyClass('home');
	 }
 ],
	subscriptions: function(params, queryParams) {
		this.register('eventsList', Meteor.subscribe('homeEventsList'));
	},
	action: ()=>{
		FlowLayout.render('HomeLayout', {header: 'Header', siderbar: 'Siderbar', main: 'MainContent'});
	}
});

var userRouter = FlowRouter.group({
	prefix: '/user',
	triggersEnter: [
		(context, redirect) => {
			if( Meteor.userId() == null) {
				FlowRouter.go('/');
			} else {
				Blaze.addBodyClass('home');
			}
		}
	],
})

userRouter.route('/', {
	action: () => {
		FlowLayout.render('HomeLayout', {header: 'Header', siderbar: 'Siderbar', main: 'UserLayout'});
	}
})

userRouter.route('/profile', {
	subscriptions: function(params, queryParams) {
		this.register('userProfile', Meteor.subscribe("userProfile"));
	},
	action: () => {
		FlowLayout.render('HomeLayout', {header: 'Header', siderbar: 'Siderbar', main: 'UserProfile'});
	}
})

FlowRouter.route('/events/:slug', {
	triggersEnter: [
	 (context, redirect) => {
		GoogleMaps.load();
	 	Blaze.addBodyClass('home');
	 }
	],
	subscriptions: function(params, queryParams) {
		var slug = params.slug || 0;
		this.register('eventsAndGameList', Meteor.subscribe('homeEventDetailList', slug));
	},
	action: function(){
		FlowLayout.render('HomeLayout', {header: 'Header', siderbar: 'Siderbar', main: 'EventDetailTemplate'});
	}
});

/**
* User it lately
**/


// FlowRouter.route('/matches/:id', {
// 	triggersEnter: [
// 	 (context, redirect) => {
// 	 	Blaze.addBodyClass('home');
// 	 }
// 	],
// 	subscriptions: function(params, queryParams) {
// 		var slug = params.id || 0;
// 		this.register('matchDetail', Meteor.subscribe('homeMatchDetail', slug));
// 	},
// 	action: function(){
// 		FlowLayout.render('HomeLayout', {header: 'Header', siderbar: 'Siderbar', main: 'MatchDetailTemplate'});
// 	}
// });

// FlowRouter.route('/games', {
// 	triggersEnter: [
// 	 (context, redirect) => {
// 	 	Blaze.addBodyClass('home');
// 	 }
// 	],
// 	subscriptions: function() {
// 		this.register('gamesList', Meteor.subscribe('homeGamesList'));
// 	},
// 	action: function() {
// 		FlowLayout.render('HomeLayout', {header: 'Header', siderbar: 'Siderbar', main: 'GamesListTemplate'});
// 	}
// })
//
// FlowRouter.route('/games/:slug', {
// 	triggersEnter: [
// 	 (context, redirect) => {
// 	 	Blaze.addBodyClass('home');
// 	 }
// 	],
// 	subscriptions: function(params, queryParams) {
// 		var slug = params.slug || false;
// 		console.log(slug);
// 		if (slug)
// 			this.register('gameAndMatchList', Meteor.subscribe('gameAndMatchList', slug));
// 	},
// 	action: function() {
// 		FlowLayout.render('HomeLayout', {header: 'Header', siderbar: 'Siderbar', main: 'MatchInGameListTemplate'});
// 	}
// })
