// Change to flow router

// Set up not found
FlowRouter.notFound = {
	action() {
		FlowLayout.render('notFoundTempalte');
	}
}

// set up admin group
var adminRouter = FlowRouter.group({
	prefix: '/admin',
	triggersEnter: [
		(context, redirect) => {
			if (!Meteor.isServer && !Meteor.userId() && !Meteor.loggingIn()) {
			    let loggedInUser = Meteor.userId();
			    redirect('/admin/login');
			}
			Blaze.addBodyClass('admin');
		}
	]
});

// admin
adminRouter.route('/', {
	action: () => {
		FlowLayout.render('AdminLayout', {adminHeader: 'AdminHeader', adminSiderbar: 'AdminSiderbar', adminTopmenu: 'AdminTopMenu', main: 'dashboardTemplate'});
	}
})

// user
adminRouter.route('/user', {
	subscriptions: function (params, queryParams) {
		var pages = 0;
		if (!_.isEmpty(queryParams)) {
			console.log('empty');
			pages = parseInt(queryParams.page) - 1;
		}
		var limit = 3;
		var offset = pages*limit;
		this.register('usersList', Meteor.subscribe('adminUserList', offset, limit));
		this.register('userImage', Meteor.subscribe('adminUserImage'))
	},
	action: (params, queryParams) => {
		FlowLayout.render('AdminLayout', {adminHeader: 'AdminHeader', adminSiderbar: 'AdminSiderbar', adminTopmenu: 'AdminTopMenu', main: 'AdminUserView'});
	}
});

//games
adminRouter.route('/games', {
	subscriptions: function(params, queryParams) {
		var options = App.getPaginationConfig(queryParams, 3);
		this.register('gameList', Meteor.subscribe('adminGamesListCom', options.offset, options.limit));
		this.register('galleryImageList', Meteor.subscribe('galleryImagesList'));
	},
	action: () => {
		FlowLayout.render('AdminLayout', {adminHeader: 'AdminHeader', adminSiderbar: 'AdminSiderbar', adminTopmenu: 'AdminTopMenu', main: 'AdminGameView'});
	}
});

adminRouter.route('/games/:action', {
	subscriptions: function(params, queryParams) {
		if(typeof queryParams.id != undefined )
			this.register('gameInfo', Meteor.subscribe('adminGameInfo', queryParams.id));
	},
	action: () => {
		FlowLayout.render('AdminLayout', {adminHeader: 'AdminHeader', adminSiderbar: 'AdminSiderbar', adminTopmenu: 'AdminTopMenu', main: 'AdminGamesForm'});
	}
});


// evetns
adminRouter.route('/events', {
	subscriptions: function(params, queryParams) {
		var options = App.getPaginationConfig(queryParams, 3);
		this.register('eventsList', Meteor.subscribe('adminEventsListCom', options.offset, options.limit));
		this.register('galleryImageList', Meteor.subscribe('galleryImagesList'));
	},
	action: () => {
		FlowLayout.render('AdminLayout', {adminHeader: 'AdminHeader', adminSiderbar: 'AdminSiderbar', adminTopmenu: 'AdminTopMenu', main: 'AdminEventView'});
	}
});

adminRouter.route('/events/:action', {
	subscriptions: function(params, queryParams) {
		var id = queryParams.id || false;
		if( id )
			this.register('adminEventInfo', Meteor.subscribe('adminEventInfo', id));
		this.register('matchesList', Meteor.subscribe('adminMatchesList'));
		this.register('gamesList', Meteor.subscribe('adminGamesList'));
	},
	action: () => {
		FlowLayout.render('AdminLayout', {adminHeader: 'AdminHeader', adminSiderbar: 'AdminSiderbar', adminTopmenu: 'AdminTopMenu', main: 'AdminEventForm'});
	}
});

// matches
adminRouter.route('/matches', {
	subscriptions: function(params, queryParams) {
		var options = App.getPaginationConfig(queryParams, 3);
		this.register('matchesList', Meteor.subscribe('adminMatchesList', options.offset, options.limit));
	},
	action: () => {
		FlowLayout.render('AdminLayout', {adminHeader: 'AdminHeader', adminSiderbar: 'AdminSiderbar', adminTopmenu: 'AdminTopMenu', main: 'AdminMatchesView'});
	}
});

adminRouter.route('/matches/:action', {
	subscriptions: function(params, queryParams) {
		var id = queryParams.id || false;
		if( id )
			this.register('matchInfo', Meteor.subscribe('adminMatchInfo', id));
		this.register('countiresList', Meteor.subscribe('countiresList'));
		this.register('gamesList', Meteor.subscribe('adminGamesList'));
		this.register('playersList', Meteor.subscribe('adminPlayersList'))
		this.register('teamsList', Meteor.subscribe('adminTeamsList'))

	},
	action: () => {
		FlowLayout.render('AdminLayout', {adminHeader: 'AdminHeader', adminSiderbar: 'AdminSiderbar', adminTopmenu: 'AdminTopMenu', main: 'AdminMatchForm'});
	}
});

// teams
adminRouter.route('/teams', {
	subscriptions: function(params, queryParams) {
		var options = App.getPaginationConfig(queryParams, 3);
		this.register('teamsList', Meteor.subscribe('adminTeamsListCom', options.offset, options.limit));
		this.register('galleryImageList', Meteor.subscribe('galleryImagesList'));
	},
	action: () => {
		FlowLayout.render('AdminLayout', {adminHeader: 'AdminHeader', adminSiderbar: 'AdminSiderbar', adminTopmenu: 'AdminTopMenu', main: 'AdminTeamView'});
	}
});

adminRouter.route('/teams/:action', {
	subscriptions: function(params, queryParams) {
		if(typeof queryParams.id != undefined )
			this.register('teamInfo', Meteor.subscribe('adminTeamInfo', queryParams.id));
		this.register('countiresList', Meteor.subscribe('countiresList'));
		this.register('gamesList', Meteor.subscribe('adminGamesList'));
		this.register('playersList', Meteor.subscribe('adminPlayersList'))
	},
	action: () => {
		FlowLayout.render('AdminLayout', {adminHeader: 'AdminHeader', adminSiderbar: 'AdminSiderbar', adminTopmenu: 'AdminTopMenu', main: 'AdminTeamForm'});
	}
});


// players

adminRouter.route('/players', {
	subscriptions: function(params, queryParams) {
		var options = App.getPaginationConfig(queryParams, 3);
		this.register('playersList', Meteor.subscribe('adminPlayersListCom', options.offset, options.limit));
		this.register('galleryImageList', Meteor.subscribe('galleryImagesList'));
	},
	action: () => {
		FlowLayout.render('AdminLayout', {adminHeader: 'AdminHeader', adminSiderbar: 'AdminSiderbar', adminTopmenu: 'AdminTopMenu', main: 'AdminPlayerView'});
	}
});

adminRouter.route('/players/:action', {
	subscriptions: function(params, queryParams) {
		var id = queryParams.id;
		if(typeof id != undefined )
			this.register('playerInfo', Meteor.subscribe('adminPlayerInfo', id));
		this.register('countiresList', Meteor.subscribe('countiresList'));
	},
	action: () => {
		FlowLayout.render('AdminLayout', {adminHeader: 'AdminHeader', adminSiderbar: 'AdminSiderbar', adminTopmenu: 'AdminTopMenu', main: 'AdminPlayerForm'});
	}
});

// gallery
adminRouter.route('/gallery', {
	subscriptions: function(params, queryParams) {
		// var options = App.getPaginationConfig(queryParams, 3);
		// this.register('galerryImageList', Meteor.subscribe('galleryImageList', options.offset, options.limit));
		 this.register('galleryImageList', Meteor.subscribe('galleryImagesList'));
	},
	action: () => {
		FlowLayout.render('AdminLayout', {adminHeader: 'AdminHeader', adminSiderbar: 'AdminSiderbar', adminTopmenu: 'AdminTopMenu', main: 'GalleryManagerView'});
	}
});


// login
FlowRouter.route('/admin/login', {
	triggersEnter: [
		(context, redirect) => {
			if (!Meteor.isServer && Meteor.userId()) {
				var loggedInUser = Meteor.userId();
			    if (Roles.userIsInRole(loggedInUser, ['admin', 'editor'])) {
				    redirect('/admin');
				}
			}
			Blaze.addBodyClass('admin');
		}
	],
    action: () => {
    	FlowLayout.render('login');
    }
});

// // router config
// Router.configure({
// 	loadingTempalte: "loadingTempalte",
// 	notFoundTemplate: "notFoundTempalte" // global not found template
// })

// // admin router

// Router.route('admin', {
// 	// layoutTemplate: "AdminLayout",
// 	controller: 'AdminController',
// 	template: 'dashboardTemplate',
// 	// action: 'render'
// });

// Router.route('admin/user', {
// 	// layoutTemplate: "AdminLayout",
// 	controller: 'AdminController',
// 	template: 'AdminUserView',
// 	// action: 'render'
// });

// Router.route('admin/games', {
// 	// layoutTemplate: "AdminLayout",
// 	controller: 'AdminController',
// 	template: 'AdminGameView',
// 	// action: 'render'
// });

// Router.route('admin/events', {
// 	controller: 'AdminController',
// 	template: 'AdminEventView'
// });

// Router.route('admin/matches', {
// 	controller: 'AdminController',
// 	template: 'AdminMatcheView'
// });

// Router.route('admin/teams', {
// 	controller: 'AdminController',
// 	template: 'AdminTeamView'
// });

// Router.route('admin/players', {
// 	controller: 'AdminController',
// 	template: 'AdminPlayerView'
// });

// Router.route('admin/login' , {
// 	controller: 'LoginController',
// 	action: 'show'
// });
