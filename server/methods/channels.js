Meteor.methods({
  deleteChannel:function(id, slug){
    check(id, String);
    Channels.remove({_id: id}, function(err, doc) {
      var event = Events.findOne({slug: slug})
      Events.update({_id: event._id}, {$pull: {channelsId: id}}, function(err){
        if(err) {
          console.log(err);
        }
      });
    })
  }
});
