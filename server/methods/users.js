Meteor.methods({
	deleteUserInAdmin: function(userId) {
		var user = Meteor.users.findOne({_id: userId});
		if(typeof user.profile.images != undefined)
			UserImage.remove({_id: user.profile.images});
		Meteor.users.remove({_id: userId});
	},
});
