/**
 * Add data for countries and region 
 */

Meteor.startup( () => {
		if(Countries.find().count() === 0) {
			console.log("Add Countries");
			var data = JSON.parse(Assets.getText("countries_data/countries.json"));
			_.each(data, (item, index) => {
				Countries.insert(item);
			})
		}
	}
);