"use strict";
// Start using ES6
/**
 * Add sample data user with role in startup
 */

Meteor.startup(() => {
	// create list user
	var user_count = Meteor.users.find().count();
	if (user_count === 0) {

		var users =
			[
		      {username:"hello1", email:"normal@example.com", roles:['user']},
		      {username:"hello2", email:"editor@example.com",roles:['editor']},
		      {username:"hello3", email:"admin@example.com",roles:['admin']}
		    ];

		var createUser = user => {
			var id;
			id = Accounts.createUser({
				username: user.username,
				email: user.email,
				password: 'admin123',
			});

			if (user.roles.length > 0) {
				Roles.addUsersToRoles(id, user.roles);
			}
		}
		// for .. of es2-15
		for (var user of users) {
			createUser(user);
		}
	}
});
