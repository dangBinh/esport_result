/**
 * This code will desecrated
 */

Meteor.publish('adminTeamsList', function(offset, limit) {
	Counts.publish(this, 'admimTeamsPage', Teams.find({}));
	var options = {};
	options.sort = {
		created: 1
	};
	options.limit = limit || 0;
	options.skip = offset || 0;
	return Teams.find({}, options);
});

Meteor.publishComposite('adminTeamsListCom', function(offset, limit){
	return {
		find: function() {
			Counts.publish(this, 'admimTeamsPage', Teams.find({}));
			var options = {};
			options.sort = {
				created: 1
			};
			options.limit = limit || 0;
			options.skip = offset || 0;
			return Teams.find({}, options);
		},
		children: [
			{
				find: function(game) {
					return Meteor.galleryMedia.find({_id : game.image})
				}
			}
		]
	}
})


Meteor.publish("adminTeamInfo", function(id){
	return Teams.find({_id: id});
});
