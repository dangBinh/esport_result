/**
 * This code will be desecrated
 */

Meteor.publish('adminGamesList', function(offset, limit) {
	Counts.publish(this, 'adminGamesPage', Games.find({}));
	var options = {};
	options.sort = {
		created: 1
	};
	options.limit = limit || 0;
	options.skip = offset || 0;
	return Games.find({}, options);
});

Meteor.publishComposite('adminGamesListCom', function(offset, limit){
	return {
		find: function() {
			Counts.publish(this, 'adminGamesPage', Games.find({}));
			var options = {};
			options.sort = {
				created: 1
			};
			options.limit = limit || 0;
			options.skip = offset || 0;
			return Games.find({}, options);
		},
		children: [
			{
				find: function(game) {
					return Meteor.galleryMedia.find({_id : game.image})
				}
			}
		]
	}
})

Meteor.publish("adminGameInfo", function(id){
	return Games.find({_id: id});
});

Meteor.publish("homeGamesList", function(){
	return Games.find({});
});

Meteor.publishComposite('gameAndMatchList', function(slug){
	return {
		find: function() {
			console.log(slug);
			return Games.find({slug: slug});
		},
		children: [
			{
				find: function(game) {
					return Matches.find({game: game._id});
				},
				children: [
					{
						find: function(match, game) {
							if(match.matchType == 2)
								return Teams.find({_id: {$in: match.teams}});
							else
								return Players.find({_id: {$in: match.teams}});
						}
					}
				]
			}
		]
	}
})
