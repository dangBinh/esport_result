Meteor.publish('adminPlayersList', function(offset, limit) {
	Counts.publish(this, 'playersList', Players.find({}));
	var options = {};
	options.sort = {
		created: 1
	};
	options.limit = limit || 0;
	options.skip = offset || 0;
	return Players.find({}, options);
});

Meteor.publishComposite('adminPlayersListCom', function(offset, limit){
	return {
		find: function() {
			Counts.publish(this, 'playersList', Players.find({}));
			var options = {};
			options.sort = {
				created: 1
			};
			options.limit = limit || 0;
			options.skip = offset || 0;
			return Players.find({}, options);
		},
		children: [
			{
				find: function(game) {
					return Meteor.galleryMedia.find({_id : game.image})
				}
			}
		]
	}
})


Meteor.publish("adminPlayerInfo", function(id){
	return Players.find({_id: id});
});
