Meteor.publish('adminMatchesList', function(offset, limit) {
	Counts.publish(this, 'admimMatchesPage', Matches.find({}));
	var options = {};
	options.sort = {
		created: 1
	};
	options.limit = limit || 0;
	options.skip = offset || 0;
	return Matches.find({}, options);
});

Meteor.publish("adminMatchInfo", function(id){
	return Matches.find({_id: id});
});
Meteor.publishComposite("homeMatchDetail", function(slug) {
	return {
		find: function() {
			return Matches.find({_id: slug})
		},
		children: [
			{
				find: function(match) {
					if(match.matchType == 2)
						return Teams.find({_id: {$in: match.teams}});
					else
						return Players.find({_id: {$in: match.teams}});
				}
			},
			{
				find: function(match) {
					var channels = match.channelsId || [];
					if(channels.length != 0)
						return Channels.find({_id: {$in: match.channelsId}});
				}
			}
		]
	}
});
