Meteor.publish("adminUserList", function(offset, limit) {
	Counts.publish(this, 'userPages', Meteor.users.find({_id: {$ne: this.userId}}));
	var options = {};
	options.sort = {
		created: 1
	};
	options.limit = limit || 0;
	options.skip = offset || 0;
	return Meteor.users.find({_id: {$ne: this.userId}}, options);
})
Meteor.publish("adminUserImage", function(){
	return UserImage.find({});
});

Meteor.publish('userProfile', function(){
	return UserImage.find({owner : this.userId});
})
