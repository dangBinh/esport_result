/**
 * This code will be despecrated
 */

Meteor.publish('adminEventsList', function(offset, limit) {
	Counts.publish(this, 'adminEventsPage', Events.find({}));
	var options = {};
	options.sort = {
		created: 1
	};
	options.limit = limit || 0;
	options.skip = offset || 0;
	return Events.find({}, options);
});

Meteor.publishComposite('adminEventsListCom', function(offset, limit){
	return {
		find: function() {
			Counts.publish(this, 'adminEventsPage', Events.find({}));
			var options = {};
			options.sort = {
				created: 1
			};
			options.limit = limit || 0;
			options.skip = offset || 0;
			return Events.find({}, options);
		},
		children: [
			{
				find: function(event) {
					return Meteor.galleryMedia.find({_id : event.image})
				}
			}
		]
	}
})

Meteor.publish("adminEventInfo", function(id){
	return Events.find({_id: id});
});

// home
Meteor.publishComposite("homeEventsList", function(){
	return {
		find: function() {
			return Events.find({});
		},
		children: [
			{
				find: function(event) {
					return Meteor.galleryMedia.find({_id : event.image})
				}
			}
		]
	}
});

Meteor.publishComposite("homeEventDetailList", function(slug) {
	return {
		find: function() {
			return Events.find({slug: slug});
		},
		children: [
			// It will be bug
			// {
			// 	find: function(event) {
			// 		return Matches.find({_id: {$in: event.matchesId}})
			// 	},
			// 	children: [
			// 		{
			// 			find: function(match, event) {
			// 				if(match.matchType == 2)
			// 					return Teams.find({_id: {$in: match.teams}});
			// 				else
			// 					return Players.find({_id: {$in: match.teams}});
			// 			}
			// 		},
			// 	]
			// },
			{
				find: function(event) {
					var channels = event.channelsId || [];
					if(channels.length != 0)
						return Channels.find({_id: {$in: event.channelsId}});
				}
			},
			{
				find: function(event) {
					return Comments.find({_id : event.commentsId});
				},
				children: [
					{
						find: function(comment, event) {
							return Meteor.users.find();
						},
						children: [
							{
								find: function(user, comment, event) {
									return UserImage.find({owner : user._id})
								}
							}
						]
					}
				]
			}
		]
	}

});
