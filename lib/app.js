/**
 * Define global object to use methods and properties want to use
 * @type {[type]}
 */

if (typeof App == undefined) {
	App = {}
}
App = {};

App.getPaginationConfig = function(queryParams ,limit) {
	var pages = 0;
	if (!_.isEmpty(queryParams)) {
		console.log('empty');
		pages = parseInt(queryParams.page) - 1;
	}
	var limit = 3;
	var offset = pages*limit;
	var options = {};
	options.limit = limit;
	options.offset = offset;
	return options;
};

App.homeNavMenu = [
	// {title: "Events", url: "/events", icon: "fa-bookmark"},
	{title: 'About', url: "/about", icon: "fa-bullseye"},
	{title: 'Options', url: "/options", icon: 'fa-cog'}
];

App.homeNavMenuLogin = [
	{title: "Profile", url: "/user/profile", icon: "fa-user"},
	{title: 'About', url: "/about", icon: "fa-bullseye"},
	{title: 'Options', url: "/options", icon: 'fa-cog'}
];

App.noImageLink = 'things/no-image-featured-image.png';
