Players = new Mongo.Collection('gm_players');

PlayerSchema = new SimpleSchema({
	nickName: {
		type: String 
	}, 
	realName: {
		type: String,
		optional: true, 
	},
	image: {
		type: String,
		optional: true
	}, 
	country: {
		type: String, 
		autoform: {
			options: function() {
		        return Countries.find().map(function (c) {
		          	return {label: c.name.common, value: c.cca2};
		        });
			}
    	}
	}
});

Players.attachSchema(PlayerSchema );

if(Meteor.isServer) {
    Players.allow({
        insert: function(userId) {
            return true;
        }, 
        update: function(userId) {
            return true;
        }, 
        remove: function(userId) {
            return true;
        }
    })
}