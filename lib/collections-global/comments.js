Comments = new Mongo.Collection('gm_comments');

CommentSchema = new SimpleSchema({
	comments: {
		type: [Object],
		optional: true
	},
	'comments.$.owner': {
		type: String,
		autoValue: function () {
			return Meteor.userId();
		},
	},
	'comments.$.createdAt': {
		type: Date,
		autoValue: function() {
			return new Date;
    }
	},
	'comments.$.content': {
		type: String
	},
	'comments.$.replies': {
		type: [Object],
		optional: true
	},
	'comments.$.replies.$.owner': {
		type: String,
		autoValue: function () {
			return Meteor.userId();
		},
	},
	'comments.$.replies.$.content': {
		type: String
	},
	'comments.$.replies.$.createdAt' : {
		type: Date,
		autoValue: function() {
			return new Date;
		}
	}
});

Comments.attachSchema(CommentSchema);

Comments.allow({
	insert: function(userId) {
			return true;
	},
	update: function(userId) {
			return true;
	},
	remove: function(userId) {
			return true;
	}
})
