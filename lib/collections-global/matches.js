Matches = new Mongo.Collection('gm_matches');

MatchesSchema = new SimpleSchema({
	matchType: {
		type: String,
		autoform: {
	      options: [
	        {label: "1.vs.1", value: "1"},
	        {label: "Team.vs.Team", value: "2"},
	      ]
	    }
	},
	game: {
		type: String,
		autoform: {
			options: function(){
				return Games.find({}).map(function(game){
					return {label: game.title, value: game._id};
				})
			}
		}
	},
	teams: {
		type: [String]
	},
	status: {
		type: String,
		autoform: {
			options: [
				{label: 'Upcoming', value: 1},
				{label: 'Live', value: 2},
				{label: 'Finished', value: 3}
			]
		}
	},
	score: {
		type: String,  // 5-3
		optional: true
	},
	startTime: {
		type: Date,
		autoform: {
		  	afFieldInput: {
	        	type: "bootstrap-datetimepicker"
	      	}
		}
	},
	round: {
		type: [Object],
		optional: true
	},
	'round.$.stats': {
		type: String, // K - D
		optional: true
	},
	'round.$.winner': {
		type: String, // mot trong 2 doi tren
		optional: true
	},
	commentId: {
		type: String,
		optional: true
	},
	// This code will be despecrated
	// channelsId: {
	// 	type: [String],
	// 	optional: true
	// }
});

Matches.attachSchema(MatchesSchema);

if(Meteor.isServer) {
    Matches.allow({
        insert: function(userId) {
            return true;
        },
        update: function(userId) {
            return true;
        },
        remove: function(userId) {
            return true;
        }
    })
}
