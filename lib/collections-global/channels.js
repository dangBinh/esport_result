Channels = new Mongo.Collection('gm_channels');

ChannelSchema = new SimpleSchema({
	channel: {
		type: Object
	},
	'channel.link': {
		type: String,
		regEx: SimpleSchema.RegEx.Url
	},
	'channel.language': {
		type: String,
		autoform: {
			options: [
				{label: 'English', value: 'en'},
				{label: 'Vietnamese', value: 'vi'}
			]
		}
	},
	'channel.ember': {
		type: String,
		optional: true
	},
	'channel.owner': {
		type: String,
    autoValue: function () {
      if (this.isInsert) {
          return Meteor.userId();
      } else if (this.isUpsert) {
          return {$setOnInsert: Meteor.userId()};
      } else {
          this.unset();
      }
		},
	  denyUpdate: true,
	},
	'channel.createAt': {
		type: Date,
		autoValue: function() {
			if (this.isInsert) {
				return new Date;
			} else if (this.isUpsert) {
				return {$setOnInsert: new Date};
			} else {
				this.unset();
			}
		}
	},
	'channel.updatedAt': {
		type: Date,
    autoValue: function() {
      if (this.isUpdate) {
        return new Date();
      }
    },
    denyInsert: true,
    optional: true
	}
});

Channels.attachSchema(ChannelSchema);

if(Meteor.isServer) {
	Channels.allow({
		insert: function(userId) {
			return true;
		},
		update: function(userId) {
			return true;
		}
	})
}
