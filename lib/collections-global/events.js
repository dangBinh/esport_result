Events = new Mongo.Collection('gm_events');

var EventSchema = new SimpleSchema({
	title: {
		type: String,
	},
	slug: {
		type: String,
		unique: true,
	},
	image: {
		type: String,
		optional: true
	},
	status: {
		type: String,
		autoform: {
			options: [
        {label: "Upcoming", value: "0"},
        {label: "Processing", value: "1"},
        {label: "Canceled", value: "2"},
				{label: "Finished", value: "3"}
      ]
		}
	},
	shedule: {
		type: [Object],
		optional: true
	},
	'schedule.$.title': {
		type: String
	},
	'schedule.$.fromDate': {
		type: Date,
		autoform: {
				afFieldInput: {
						type: "bootstrap-datetimepicker"
					}
		}
	},
	'schedule.$.toDate': {
		type: Date,
		autoform: {
		  	afFieldInput: {
	        	type: "bootstrap-datetimepicker"
	      	}
		}
	},
	matchesId: {
		type: [String],
		optional: true,
		autoform: {
			options: function(){
				return Matches.find({}).map(function(match){
					return {label: match.teams[0] + ' vs ' + match.teams[1], value: match._id};
				})
			}
		}
	},
	games: {
		type: [String],
		autoform: {
			options: function(){
				return Games.find({}).map(function(game){
					return {label: game.title, value: game._id};
				})
			}
		},
		optional: true
	},
	description: {
		type: String,
		optional: true,
		 autoform: {
		      afFieldInput: {
		          type: 'summernote',
		        class: 'editor' // optional
		      }
    	}
	},
	caster: {
		type: String,
		optional: true
	},
	pre_prize_pool: {
		type: String,
		optional: true
	},
	prize_pool: {
		type: String,
		optional: true
	},
	channelsId : {
		type: [String],
		optional: true
	},
	commentsId: {
		type: String,
		optional: true
	},
	owner: {
		type: String,
		autoValue: function () {
      if (this.isInsert) {
          return Meteor.userId();
      } else if (this.isUpsert) {
          return {$setOnInsert: Meteor.userId()};
      } else {
          this.unset();
      }
		},
	  denyUpdate: true,
	},
	location: {
		type:Object,
		autoform: {
			type: 'map',
			afFieldInput: {
				geolocation: true,
				searchBox: true,
				autolocate: true
			}
		}
	},
	'location.lat': {
		type: String
	},
	'location.lng': {
		type: String
	}
});

Events.attachSchema(EventSchema);

// if(Meteor.isServer) {
    Events.allow({
        insert: function(userId) {
            return true;
        },
        update: function(userId) {
            return true;
        },
        remove: function(userId) {
            return true;
        }
    })
// }
