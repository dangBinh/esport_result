Games = new Mongo.Collection('gm_games');

GamesSchema = new SimpleSchema({
    title: {
        type: String,
        optional: true
    },
    slug: {
        type: String,
        regEx: /^[a-z0-9A-Z-]{2,50}$/,
        unique: true
    },
    description: {
        type: String,
        optional: true, 
        autoform: {
      afFieldInput: {
          type: 'summernote',
        class: 'editor' // optional
      }
    }
    },
    image: {
        type: String,
        optional: true
    }
});

Games.attachSchema(GamesSchema);

if(Meteor.isServer) {
    Games.allow({
        insert: function(userId) {
            return true;
        }, 
        update: function(userId) {
            return true;
        }, 
        remove: function(userId) {
            return true;
        }
    })
}