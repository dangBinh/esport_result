// Add UserImage collection file

var createThumb = function(fileObj, readStream, writeStream) {
  gm(readStream, fileObj.name()).resize('30', '30').stream().pipe(writeStream);
}

// Create user Image store
var userImageStore = new FS.Store.GridFS("userImages", {

});
// Create user thumb Store
var userThumbStore =new FS.Store.GridFS("userThumb", {
  transformWrite: createThumb
})

UserImage = new FS.Collection("userImages", {
    stores: [userImageStore, userThumbStore],
    filter: {
      allow: {
        contentTypes: ['image/*']
      }
    }
});



UserImage.allow({
    insert: function(userId, doc) {
        return true;
    },
    download: function(userId) {
        return true;
    },
    update: function(userId, doc) {
        return true;
    },
    remove: function(userId) {
        return true;
    }
})

// Validate user

Schema = {};

Schema.UserCountry = new SimpleSchema({
    name: {
        type: String,
        label: 'Country'
    }
});

Schema.UserProfile = new SimpleSchema({
    firstName: {
        type: String,
        regEx: /^[a-zA-Z-]{2,25}$/,
        optional: true
    },
    lastName: {
        type: String,
        regEx: /^[a-zA-Z]{2,25}$/,
        optional: true
    },
    birthday: {
        type: Date,
        optional: true
    },
    gender: {
        type: String,
        allowedValues: ['Male', 'Female'],
        optional: true
    },
    country: {
        type: Schema.UserCountry,
        optional: true
    },
    images: {
        type: String,
        autoform: {
            afFieldInput: {
                type: "cfs-file",
                collection: 'userImages',
                label: "Choose file"
            }
        },
        optional: true
    },
    thumb: {
      type: String,
      optional: true
    },
    thumbUrl: {
      type: String,
      optional: true
    }
});

Schema.User = new SimpleSchema({
    username: {
        type: String,
        regEx: /^[a-z0-9A-Z_]{3,15}$/
    },
    emails: {
        type: [Object],
        // this must be optional if you also use other login services like facebook,
        // but if you use only accounts-password, then it can be required
        optional: true,
        label: 'Email'
    },
    "emails.$.address": {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    "emails.$.verified": {
        type: Boolean
    },
    createdAt: {
        type: Date,
        autoValue: function () {
            if (this.isInsert) {
              return new Date;
            } else {
              return new Date;
            }
        },
        optional: true
    },
    profile: {
        type: Schema.UserProfile,
        optional: true
    },
    services: {
        type: Object,
        optional: true,
        blackbox: true
    },
    // Add `roles` to your schema if you use the meteor-roles package.
    // Option 1: Object type
    // If you specify that type as Object, you must also specify the
    // `Roles.GLOBAL_GROUP` group whenever you add a user to a role.
    // Example:
    // Roles.addUsersToRoles(userId, ["admin"], Roles.GLOBAL_GROUP);
    // You can't mix and match adding with and without a group since
    // you will fail validation in some cases.
    // roles: {
    //     type: Object,
    //     optional: true,
    //     blackbox: true
    // },
    // Option 2: [String] type
    // If you are sure you will never need to use role groups, then
    // you can specify [String] as the type
    roles: {
        type: [String],
        optional: true,
        autoform: {
            options: [
                {label: "Editor", value: "editor"},
                {label: "User", value: ""},
              ]
        }
    }
});

Meteor.users.attachSchema(Schema.User);

Meteor.users.allow({
    insert: function() {
        return true;
    },
    update: function(){
        return  true;
    },
    remove: function() {
        return true;
    }
});
