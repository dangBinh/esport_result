Teams = new Mongo.Collection('gm_teams');

TeamSchema = new SimpleSchema({
	name: {
		type: String
	}, 
	image: {
		type: String,
		optional: true
	},
	subteam: {
		type: [Object]
	}, 
	'subteam.$.games': {
		type: String,
		autoform: {
			options: function() {
		        return Games.find().map(function (c) {
		          	return {label: c.title, value: c._id};
		        });
			}
		}
	}, 
	'subteam.$.players': {
		type: [String],
		autoform: {
			options: function() {
		        return Players.find().map(function (c) {
		          	return {label: c.nickName, value: c._id};
		        });
			}
		}
	}, 
	country: {
		type: String, 
		autoform: {
			options: function() {
		        return Countries.find().map(function (c) {
		          	return {label: c.name.common, value: c.cca2};
		        });
			}
		}
	}

});

Teams.attachSchema(TeamSchema);

if(Meteor.isServer) {
    Teams.allow({
	    insert: function(userId) {
	        return true;
	    }, 
	    update: function(userId) {
	        return true;
	    }, 
	    remove: function(userId) {
	        return true;
	    }
    });
}