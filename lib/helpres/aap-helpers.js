if(Meteor.isClient) {
  Template.registerHelper("parseLInkToHost", function(url){
    var a = $('<a>', {href:url})[0];
    return a.hostname + a.pathname;
  });
  Template.registerHelper("isOwner", function(id) {
    if(Meteor.userId() == id) {
      return true;
    }
    return false;
  });
}
